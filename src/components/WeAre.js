import React, {Component} from 'react';
import './WeAre.css';
import imagenNosotros from './images/meeting.jpg';
import ibm from './images/ibm.png';
import microsoft from './images/microsoft.png';
import moprosoft from './images/moprosoft.png';
import pmi from './images/pmi.png';
import titanium from './images/titanium.png';
import {Grid, Row, Col } from 'react-flexbox-grid';






class WeAre extends Component{

    render(){
return (

<Grid fluid >
<Row  around="lg" className="div-nosotros">

        {/* DIV NOSOTROS SOMOS */}
        <Col  lg={5} xl={5}  className="ml-5" >
            <img src={imagenNosotros} className="image-nosotros "/>
        </Col>
        <Col lg={5}  xl={5}  className="mr-5" >
        <h1 className="titulo-nosotros" >NOSOTROS SOMOS ISITA</h1>
        <div className="separador">
        </div>
       <p>Nuestros profesionales asumen la filosofía y modo de trabajar 
          de tu empresa para dar una respuesta profesional en tecnología 
          y recursos humanos de IT como si fueran parte de tu compañía.
       </p>
      <p>Cubrimos con nuestros servicios los tres componentes que son 
          necesarios para hacer frente a los retos en transformación 
          digital: la consultoría en transformación digital e innovación, 
          el diseño y la implantación tecnológica con la búsqueda y gestión 
          del talento.</p>
        </Col>

   
</Row>

<Row between="lg" center="lg" className="separador-abajo">
            <Col lg={2} className="imagen-separacion-pmi"><img src={pmi} className="image-pmi "/></Col>
            <Col lg={2} className="imagen-separacion"><img src={titanium} className="image-titanium "/></Col>
            <Col lg={2} className="imagen-separacion"><img src={ibm} className="image-ibm "/></Col>
            <Col lg={2} className="imagen-separacion"><img src={microsoft} className="image-microsoft"/></Col>
            <Col lg={2} className="imagen-separacion"><img src={moprosoft} className="image-moprosoft"/></Col>
    </Row>
</Grid>

); 
    }
}

export default WeAre;