import React from 'react';
import Menu from './components/Menu'
import WeAre from './components/WeAre'
import OurProducts from './components/OurProducts'
import logo from './logo.svg';
import './App.css';

function App() {
  return (
  <div>
   <Menu/>
   <WeAre/>
   <OurProducts/>
  </div> 
  );
}

export default App;
